#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cassandra.cluster import Cluster

def main(args):
    cluster = Cluster(['localhost'])
    session = cluster.connect('demo')

    rows = session.execute('select first_name, last_name, empid from emp')

    for (first_name, last_name, empid) in rows:
        print (first_name, last_name, empid)

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
