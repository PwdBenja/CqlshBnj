# GuiaCassandraBnj

## Prerrequisito

En el entorno virtual:
```bash
pip list
```

## Instalación

##### Instalar pip para python3

Antes de la instalación del entorno virtual, instalemos pip. Pip es un administrador de paquetes que ayuda a instalar, desinstalar y actualizar paquetes para sus proyectos.

Para instalar pip para python 3 escriba:
```bash
apt install python3-pip
```
Vamos primero instalar venv paquete usando el siguiente comando:

```bash
apt-get install python3-venv
```
Creamos el entorno
```bash
python3 -m venv venv=my_env_project
```
Activamos el entorno de trabajo
```bash
source my_env_project/bin/activate
```


### Paso 1: instalar Java en Ubuntu

La instalación de Apache Cassandra comienza con la verificación de si Java está instalado. Para ser más específico, OpenJDK es lo que se requiere para trabajar sin problemas con Apache Cassandra. Es más probable que la instalación de una versión diferente produzca errores durante la configuración.

Para verificar si Java está instalado, ejecute el comando:

```bash
java -version
```

Si Java aún no está instalado, encontrará la salida impresa como se muestra en su terminal.

Para instalar OpenJDK, ejecute el siguiente comando apt.

```bash
sudo apt install openjdk-8-jdk
```
Una vez más, confirme que Java está instalado ejecutando el comando.

```bash
java -version
```

Configure y marque la opcion de JDK 8 con:


```bash
update-alternatives --config java
```
#### Paso 2: Instale Apache Cassandra en Ubuntu

Con Java instalado, procederemos a instalar Apache Cassandra. Primero, instale el paquete apt-transport-https para permitir el acceso a los repositorios a través del protocolo https.


```bash
sudo apt install apt-transport-https
```

A continuación, importe la clave GPG utilizando el siguiente comando wget como se muestra.

```bash
wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
```

Luego agregue el repositorio de Apache Cassandra al archivo de lista de fuentes del sistema como se muestra.

```bash
sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 311x main" > /etc/apt/sources.list.d/cassandra.list'
```

Actualice su lista de paquetes.

```bash
sudo apt update

sudo apt upgrade
```
Luego instale la base de datos NoSQL usando el comando:

```bash
sudo apt install cassandra
```
Para iniciar el cluster debe:

```bash
sudo service cassandra start - stop - restart - status

```

```bash
cassandra

```
```bash
cqlsh
```
#### if Can not open,/var/log/cassandra/gc.log due to Permission denied

I was facing the same issue, fixed using following commands:


```bash
sudo chown -R $USER:$GROUP /var/lib/cassandra/
```
```bash
sudo chown -R $USER:$GROUP /var/log/cassandra/
```


## driver python

```bash
sudo pip install cassandra-driver
```

Ejemplo:

```python

from cassandra.cluster import Cluster

def main(args):
    cluster = Cluster(['localhost'])
    session = cluster.connect('demo')

    rows = session.execute('select first_name, last_name, empid from emp')

    for (first_name, last_name, empid) in rows:
        print (first_name, last_name, empid)

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

```
